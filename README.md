## Permutacje - porządek minimalnych zmian
Projekt został wykonany w ramach przedmiotu Algorytmy kombinatoryczne na studiach. 

## Cel
Celem projektu wybranie jednego z kilkunastu tematów, zapoznanie się z tematem i implementacja algorytmu w dowolnym języku programowania.

## Opis działania
Podajemy liczbę n. Algorytm generuje n! (n silnia) możliwych permutacji, w każdej permutacji znajduje się n + 1 cyfr. Każda kolejna perutacja powstaje po przesunięciu cyfry n o jedno miejsce. Jeśli n jest na końcu prawej lub lewej strony, w kolejne jkombinacji zostaje na swoim miejscu a zmiana następuje w środku równiez z przesunięciem o jedno miejsce. Dalej cyfra n ponownie przesuwana jest do skrajnej krawęzi.
